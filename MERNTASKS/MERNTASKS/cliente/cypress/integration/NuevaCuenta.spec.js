//Se agrega para obtener mejor autocompletado de cypress
///<reference types="cypress" />  

describe('<NuevaCuenta /> ', () => {
    it('<NuevaCuenta /> -Validacion y alertas y crear nueva cuenta', () => {

        cy.visit('http://localhost:3000/nueva-cuenta');

        cy.get('[data-cy=submit-nueva-cuenta]').click(); //se presiona click

        cy.get('[data-cy=alerta]') //se valida la alerta
            .should('have.class', 'alerta-error')

        cy.get('[data-cy=alerta]')
            .should('exist')
            .invoke('text')
            .should('equal', 'Todos los campos son obligatorios')

        cy.get('[data-cy=nombre-input]')
            .clear()
            .type('Glenn');
        cy.get('[data-cy=email-input]')
            .clear()
            .type('arias.fex@gmail.com');
        cy.get('[data-cy=password-input]')
            .clear()
            .type('123');
        cy.get('[data-cy=repetir-password-input]')
            .clear()
            .type('123');
        cy.get('[data-cy=submit-nueva-cuenta]').click(); //se presiona click

        cy.get('[data-cy=alerta]')
            .should('exist')
            .invoke('text')
            .should('equal', 'El password debe ser de al menos 6 caracteres')


        cy.get('[data-cy=nombre-input]')
            .clear()
            .type('eAgro');
        cy.get('[data-cy=email-input]')
            .clear()
            .type('sisagricola.lalydia@gmail.com');
        cy.get('[data-cy=password-input]')
            .clear()
            .type('123456');
        cy.get('[data-cy=repetir-password-input]')
            .clear()
            .type('123');
        cy.get('[data-cy=submit-nueva-cuenta]').click(); //se presiona click

        cy.get('[data-cy=alerta]')
            .should('exist')
            .invoke('text')
            .should('equal', 'Los passwords no son iguales')

            cy.get('[data-cy=nombre-input]')
            .clear()
            .type('Wendy');
        cy.get('[data-cy=email-input]')
            .clear()
            .type('sisagricola.lalydia@gmail.com');
        cy.get('[data-cy=password-input]')
            .clear()
            .type('123456');
        cy.get('[data-cy=repetir-password-input]')
            .clear()
            .type('123456');
        cy.get('[data-cy=submit-nueva-cuenta]').click(); //se presiona click

        cy.get('[data-cy=selecciona]')  
            .should('exist')

        cy.get('[data-cy=cerrar-sesion]').click();
    });

    it('<NuevaCuenta /> - Revisar usuarios duplicados',()=>{
        cy.visit('http://localhost:3000/nueva-cuenta');


        cy.get('[data-cy=nombre-input]')
            .clear()
            .type('eAgro');
        cy.get('[data-cy=email-input]')
            .clear()
            .type('sisagricola.lalydia@gmail.com');
        cy.get('[data-cy=password-input]')
            .clear()
            .type('123456');
        cy.get('[data-cy=repetir-password-input]')
            .clear()
            .type('123456');
        cy.get('[data-cy=submit-nueva-cuenta]').click(); //se presiona click

        cy.get('[data-cy=alerta]')
        .should('exist')
        .invoke('text')
        .should('equal', 'El usuario ya existe')
    })

});