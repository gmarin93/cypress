//Se agrega para obtener mejor autocompletado de cypress
///<reference types="cypress" />  
//

describe('<Formularios />', () => {
    it('<Login/> -Verificar pantalla de inicio', () => {

        cy.visit('http://localhost:3000/');

        // cy.contains('h1','Iniciar Sesión'); //No se recomienda de esta forma con data-cy sobre las etiquetas es mas recomendado
        cy.get('[data-cy=titulo]')
            .invoke('text')
            .should('equal', 'Iniciar Sesión')

        //Revisar que el formulario exista
        cy.get('[data-cy=form-login]')
            .should('exist');

        //Revisar los dos inputs
        cy.get('[data-cy=email-input]')
            .should('exist');

        cy.get('[data-cy=password-input]')
            .should('exist');

        cy.get('[data-cy=submit-login]')
            .should('exist')
            .should('have.value', 'Iniciar Sesión')
            .should('have.class', 'btn-primario')
            .should('have.class', 'btn');

        cy.get('[data-cy=nueva-cuenta')
            .should('exist')
            .should('have.prop', 'tagName')
            .should('eq', 'A')

        cy.get('[data-cy=nueva-cuenta')
            .should('have.attr', 'href')
            .should('eq', '/nueva-cuenta');

        cy.visit('http://localhost:3000/nueva-cuenta');

    });

    it('<NuevaCuenta/> -Verificar el componente de nueva cuenta', () => {

        cy.get('[data-cy=titulo]')
            .should('exist')
            .invoke('text')
            .should('equal', 'Obtener una cuenta')

        cy.get('[data-cy=nueva-cuenta]')
            .should('exist');

        cy.get('[data-cy=nombre-input]')
            .should('exist');

        cy.get('[data-cy=password-input]')
            .should('have.prop','type')
            .should('equal','password')
            .should('exist');

        cy.get('[data-cy=repetir-password-input]')
            .should('exist');

        cy.get('[data-cy=submit-nueva-cuenta]')
            .should('exist')
            .should('have.class','btn-primario')
            .should('have.value','Registrarme')
            .should('not.have.value','Crear Nueva Cuenta')
        
        cy.get('[data-cy=enlace-login]')
            .should('exist')
            .should('have.attr','href')
            .should('eq','/');
        
        cy.visit('http://localhost:3000/')
        
    });
}); //Agrupa los test