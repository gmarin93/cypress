///<reference types="cypress" />  

describe('Administrador', () => {
    it('<Login /> -Autentificacion', () => {
        cy.visit('http://localhost:3000/')

        cy.get('[data-cy=email-input]').clear().type('sisagricola.lalydia@gmail.com');
        cy.get('[data-cy=password-input]').clear().type('123456');
        cy.get('[data-cy=submit-login]').click();

    });

    it('<Proyectos /> - Validar Proyectos',()=>{
        //Validacion
        cy.get('[data-cy=boton-nuevo-proyecto]').click();
        cy.get('[data-cy=submit-nuevo-proyecto]').click();

        cy.get('[data-cy=alerta]')
        .should('exist')
        .invoke('text')
        .should('equal', 'El nombre del Proyecto es obligatorio')

        cy.get('[data-cy=alerta]')
            .should('have.class', 'mensaje error');
    });

    it('<Proyectos /> - Creacion de Proyectos',()=>{

        cy.get('[data-cy=input-nuevo-proyecto]').type('Tienda Virtual');
        cy.get('[data-cy=submit-nuevo-proyecto]').click();

        //Seleccionar el proyecto
        cy.get('[data-cy=listado-proyectos] li:nth-child(1) button').click();
    });

    it('<Tareas /> - Validacion de tareas',()=>{

        cy.get('[data-cy=submit-tarea]').click();

        cy.get('[data-cy=alerta]')
        .should('exist')
        .invoke('text')
        .should('equal', 'El nombre de la tarea es obligatorio')

        cy.get('[data-cy=alerta]')
            .should('have.class', 'mensaje error');

            //Creacion de la tarea
            cy.get('[data-cy=input-tarea]').type('Definir diseno')
            cy.get('[data-cy=submit-tarea]').click();

            cy.get('[data-cy=input-tarea]').type('Definir colores')
            cy.get('[data-cy=submit-tarea]').click();
       
    });

    it('<Tareas /> - Completar, Descompletar, Editar y Eliminar',()=>{

        //Selecciona la primer tarea y la marca como correcta
        cy.get('[data-cy=tarea]:nth-child(1) [data-cy=tarea-incompleta]').click();
        cy.get('[data-cy=tarea] [data-cy=tarea-completa]').should('have.class','completo');

        //Selecciona la primer tarea y la desmarca
        cy.get('[data-cy=tarea] [data-cy=tarea-completa]').click();
        cy.get('[data-cy=tarea] [data-cy=tarea-incompleta]').should('have.class','incompleto');

       //Edicion
       cy.get('[data-cy=tarea]:nth-child(1) [data-cy=btn-editar').click();
       cy.get('[data-cy=input-tarea]').clear().type('Tarea Actualizada');
       cy.get('[data-cy=submit-tarea]').click();


       //Eliminar tarea
       cy.get('[data-cy=tarea]:nth-child(1) [data-cy=btn-eliminar').click();
       cy.get('[data-cy=tarea]').invoke('text').should('not.equal', 'Tarea Actualizada')

    });
});
