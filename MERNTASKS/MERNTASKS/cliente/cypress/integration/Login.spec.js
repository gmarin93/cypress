///<reference types="cypress" />  

describe('<Login /> ', () => {
    it('<Login /> -Validacion y alertas y autenticar usuario', () => {

        cy.visit('http://localhost:3000/');

        cy.get('[data-cy=submit-login]').click();

        cy.get('[data-cy=alerta]')
            .should('exist')
            .invoke('text')
            .should('equal', 'Todos los campos son obligatorios')

            //Probar con un usuario que no existe
        cy.get('[data-cy=email-input]').clear().type('sisagricola.lalyfdia@gmail.com');
        cy.get('[data-cy=password-input]').clear().type('12345');
        cy.get('[data-cy=submit-login]').click();

        cy.get('[data-cy=alerta]')
        .should('exist')
        .invoke('text')
        .should('equal', 'El usuario no existe')

              //Probar con un pass erroneo existe
              cy.get('[data-cy=email-input]').clear().type('sisagricola.lalydia@gmail.com');
              cy.get('[data-cy=password-input]').clear().type('12345');
              cy.get('[data-cy=submit-login]').click();
      
              cy.get('[data-cy=alerta]')
              .should('exist')
              .invoke('text')
              .should('equal', 'Password Incorrecto')

            //Autenticar usuario

            cy.get('[data-cy=email-input]').clear().type('sisagricola.lalydia@gmail.com');
            cy.get('[data-cy=password-input]').clear().type('123456');
            cy.get('[data-cy=submit-login]').click();

            cy.get('[data-cy=cerrar-sesion]').click();

    });
});
